import markdown

md = markdown.Markdown(extensions=[
  'markdown.extensions.extra',
  'markdown.extensions.meta',
  'markdown.extensions.codehilite',
  'markdown.extensions.smarty',
  'markdown.extensions.sane_lists',
  'markdown.extensions.fenced_code',
  'markdown.extensions.toc',
  ], output_format='html5')
