
# Tech Dispatches

## Objectives

1. Curate a knowledge management repository for my personal use.
2. Deploy it with minimal Gramex (+Tornado).

## Criteria

- no JavaScript
- minimal overhead to maintain
- content only on the technology I've contributed to or worked with

It's very comfortable to rely on JavaScript, jQuery for basic tasks on the web. I wanted to challenge myself to create a site without using JavaScript.

As I wanted Python support for the application, I picked Heroku over other front-end systems (surge.sh) I used in the past.

# Deployment

This app is deployed on a heroku instance via a Procfile: [https://tech-dispatches.herokuapp.com/](https://tech-dispatches.herokuapp.com/)

If you are unfamiliar with Procfile (as I was when I did this) [Heroku Dev Center](https://devcenter.heroku.com/articles/procfile) has you covered.
