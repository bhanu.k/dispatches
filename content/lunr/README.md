# Lunr.js

lunr is a small library to support search in documents. Know more at the [Lunr](https://lunrjs.com/) website.

## Dispatch

This Dispatch will illustrate Lunr.js for markdown file-based documentation search using [Gramex Guide](https://learn.gramener.com/guide/search/) as a reference.

## Goal

Our goal is to create a `searchindex.json` file that's used to lookup using user input text and present results.

```json
{
  "docs": [
    {
      "title": "Admin page",
      "prefix": "Admin",
      "link": "admin/#admin-page",
    },
    ...
  ],
  "index": {
    "fields": ["title", "body", "id"],
    "fieldVectors": [...],  // auto-generated
    "invertedIndex": [],    // auto-generated
    "pipeline": []          // auto-generated
  }
}
```
