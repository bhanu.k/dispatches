# Gramex

Gramex is a Tornado-based web platform for data science applications. Know more at [Gramex](https://github.com/gramener/gramex/) GitHub repository.

## Dispatch

This Dispatch will illustrate email sending, YAML anchors, YAML conditionals to control configurations.
